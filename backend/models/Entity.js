import mongoose from 'mongoose'

const Schema = mongoose.Schema

const entitySchema = new Schema({
  name: String,
  subEntities: [{
    name: String
  }],
  feedbacks: [{
    feedback: String,
    stars: Number
  }]
})

const Entity = mongoose.model('Entity', entitySchema)

export default Entity
