import Entity from '../models/Entity'

export const getEntity = async (request, response) => {
  const condition = !request.query.q ? {} : {name: { "$regex": `.*${request.query.q}.*`}}
  response.json( await Entity.find(condition, 'name').limit(20))
}

export const getSubEntity = async (request, response) => {
  !request.query.q
    ? response.json( await Entity.findById(request.params.id))
    : await Entity.findById(request.params.id).then((data) => {
        response.json({subEntities: data.subEntities.filter(each => new RegExp(request.query.q).test(each.name))})
      })
}

export const postFeedbacks = async (request, response) => {
  const update = {'$push': {feedbacks: request.body}}
  response.json( await Entity.findByIdAndUpdate(request.params.id, update))
}
