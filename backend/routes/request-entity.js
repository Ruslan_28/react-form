const router = require('express-promise-router')()

import { getEntity, getSubEntity, postFeedbacks } from '../controllers/entity'

router.get(
  '/getEntity',
  getEntity
)

router.get(
  '/getSubEntity/:id',
  getSubEntity
)

router.post(
  '/postFeedbacks/:id',
  postFeedbacks
)

export default router
