import React, { Component } from 'react'
import './App.css';
import SelectAutocomplete from './components/SelectAutocomplete'
import Form from './components/Form'

class App extends Component {
  state = {
    firstValue: null,
    secondValue: null,
    id: null
  }

  changeFirstValue = (value, id) => {
    this.setState({ firstValue: value, id: id })
  }

  changeSecondValue = (value) => {
    this.setState({ secondValue: value })
  }

  render() {
    const { firstValue, secondValue, id } = this.state

    return (
      <>
        <SelectAutocomplete
          changeFirstValue={this.changeFirstValue}
          changeSecondValue={this.changeSecondValue}
          firstValue={firstValue}
        />
        {firstValue && secondValue && secondValue.length > 0 && (
          <Form id={id} />
        )}
      </>
    );
  }
}

export default App;
