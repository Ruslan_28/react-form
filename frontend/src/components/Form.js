import React, { Component } from 'react'
import axios from 'axios'
import styled from 'styled-components'
import { Input, Rate, Button } from 'antd'
import 'antd/dist/antd.css';

const Style = styled.div`
  max-width: 500px;
  width: 100%;
  margin: 0 auto;

  h3 {
    margin-bottom: 10px;
    margin-top: 15px;
  }

  button {
    display: block;
    margin-top: 25px;
  }
`

class Form extends Component {
  state = {
    stars: null,
    feedback: null
  }

  handleRate = (value) => {
    this.setState({ stars: value })
  }

  handleTextarea = (e) => {
    const value = e.target.value

    this.setState({ feedback: value })
  }

  handleSubmit = (e) => {
    e.preventDefault()

    const { stars, feedback } = this.state
    const { id } = this.props

    const data = { stars, feedback }

    axios.post(`http://localhost:8000/entity/postFeedbacks/${id}`, data)
      .then(() => console.log('success!!!'))
      .catch(err => console.error(err))
    }

  render() {
    const { TextArea } = Input;
    const { stars, feedback } = this.state;

    return (
      <Style>
        <h3>Feedback</h3>
        <form>
          <TextArea
            value={feedback}
            placeholder="Write feedback"
            rows={4}
            onChange={this.handleTextarea}
          />
          <h3>Rate</h3>
          <Rate
            value={stars}
            onChange={this.handleRate}
          />
          <Button onClick={this.handleSubmit} type="submit">Submit</Button>
        </form>
      </Style>
    );
  }
}

export default Form;
