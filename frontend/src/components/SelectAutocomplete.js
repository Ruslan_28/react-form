import React, { Component } from 'react'
import axios from 'axios'
import styled from 'styled-components'
import { Select } from 'antd'
import 'antd/dist/antd.css';

const Style = styled.div`
  max-width: 500px;
  width: 100%;
  margin: 0 auto;
  margin-bottom: 35px;

  h1 {
    text-align: center;
  }

  .ant-select {
    width: 100%;
    margin-top: 25px;
  }
`

class SelectAutocomplete extends Component {
  state = {
    firstList: [],
    secondList: [],
  }

  componentDidMount() {
    axios.get('http://localhost:8000/entity/getEntity')
      .then((response) => this.setState({ firstList: response.data }))
      .catch(error => console.error(error))
  }

  handleChange = (value, index) => {
    const id = index.key

    axios.get(`http://localhost:8000/entity/getSubEntity/${id}`)
      .then((response) => {
        this.setState({ secondList: response.data.subEntities })
        this.props.changeFirstValue(value, id)
      })
      .catch(error => console.error(error))
  }

  render() {
    const { Option } = Select
    const { firstList, secondList } = this.state
    const { changeSecondValue, firstValue } = this.props

    return (
      <Style>
        <h1>React form</h1>
        <Select
          showSearch
          placeholder="Select a entity"
          optionFilterProp="children"
          onChange={this.handleChange}
          filterOption={(input, option) =>
            option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
          }
        >
          {firstList.map(option => <Option key={option._id} value={option.name}>{option.name}</Option>)}
        </Select>
        {
          firstValue &&
          <Select
            mode="multiple"
            showSearch
            placeholder="Select a entity"
            optionFilterProp="children"
            onChange={changeSecondValue}
            filterOption={(input, option) =>
              option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
            }
          >
            {secondList.map(option => <Option key={option._id} value={option.name}>{option.name}</Option>)}
          </Select>
        }
      </Style>
    )
  }
}

export default SelectAutocomplete;
